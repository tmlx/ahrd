#submit to cluster: snakemake -s ahrd_v4.snakefile --cluster "qsub -N {params.job_name} -V -S /bin/bash -e {params.loge} -o {params.logo} -q plant2,plant_wheat -pe serial {threads} -l job_mem={resources.M}" --jobs 100 --restart-times 3 --latency-wait 300
#submit to slurm: snakemake -s ahrd_v4.snakefile --cluster "sbatch -p pgsb -N 1 -n {threads} -J {params.job_name} -e {params.loge} -o {params.logo}" --jobs 100 --restart-times 1 --latency-wait 120

#[25/10/2018] changed mem_mb to M and corrected the amount of memory requested from e.g. 1000 to 1000000000 (because it is passed along as bytes)
#[24/09/2019] added command line for slurm
#[26/09/2019] added rules for combining and parsing ahrd
#[27/07/2020] replaced blast with diamond


import Bio.SeqIO
from itertools import chain, islice


def chunks(iterable, size=10):
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, size - 1))

configfile: "ahrd.config.yaml"

SPROT_DB = config["databases"]["sprot"]["db"]
TAIR_DB = config["databases"]["tair"]["db"]
TREMBL_DB = config["databases"]["trembl_plants"]["db"]
QUERY =    config["data"]["input"]
CHUNKS = config["data"]["chunks"]

BATCHES = int((len(list(Bio.SeqIO.parse(QUERY,"fasta")))/CHUNKS)+1)



PREFIX_QUERY = os.path.split(QUERY)[1]
QUERY_NOSTAR = PREFIX_QUERY+".nostar.fasta"
WRK_DIR = os.getcwd()
STDOUT =  WRK_DIR


#print(DATABASE.keys())

rule all:
    input:
        #expand(
        #    "blast_sprot_out/blast.{chunk_id}.sprot.blp",
        #    chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        #dynamic("blast_sprot_out/blast.{chunk_id}.sprot.blp"),
        #dynamic("blast_tair_out/blast.{chunk_id}.tair.blp"),
        #dynamic("blast_trembl_out/blast.{chunk_id}.trembl.blp"),
        #"blast."+PREFIX_QUERY+"_sprot.blp",
        #"blast."+PREFIX_QUERY+"_tair.blp",
        #"blast."+PREFIX_QUERY+"_trembl.blp",
        #"interpro."+PREFIX_QUERY+".out",
        #expand("yaml_out/"+QUERY+".{chunk_id}.run_ahrd.yaml",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        expand("ahrd_out/"+QUERY+".{chunk_id}.ahrd.csv", chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        PREFIX_QUERY+".ahrd.csv"


rule prepare:
    input:
        QUERY
    output:
        temp(QUERY_NOSTAR)
    params:
        job_name = "Prep",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    shell:
        "sed s'/*$//' "
        "{input} "
        "> {output}"


rule chunk:
    input: QUERY_NOSTAR
    output:
        expand("tmp/fasta.{chunk_id}.fa", chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)])
    params:
        job_name = "Chunking",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    run:
        sio = Bio.SeqIO.parse(QUERY_NOSTAR, 'fasta')
        for cnt, chunk in enumerate(chunks(sio, CHUNKS)):
            with open("tmp/fasta.{0:05d}.fa".format(cnt), 'w') as out:
                Bio.SeqIO.write(chunk, out, 'fasta')


rule blast_sprot:
    input:
        database = SPROT_DB+".dmnd",
        query = "tmp/fasta.{chunk_id}.fa",
    output:
        "blast_sprot_out/blast.{chunk_id}.sprot.blp"
    params:
        job_name = "Blofelding",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 4000000000
    shell:
        "diamond "
        "blastp "
        "--db {input.database} "
        "--more-sensitive "
        "--query {input.query} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "


rule combine_sprot:
    input:
        expand(
            "blast_sprot_out/blast.{chunk_id}.sprot.blp",
            chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)])
    output:
        "blast."+PREFIX_QUERY+"_sprot.blp"
    params:
        job_name = "Combining",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    shell:
        "find blast_sprot_out/ -name \"*.blp\" -exec cat {{}} \; > {output}"


rule blast_tair:
    input:
        database = TAIR_DB+".dmnd",
        query = "tmp/fasta.{chunk_id}.fa",
    output:
        "blast_tair_out/blast.{chunk_id}.tair.blp"
    params:
        job_name = "Blofelding",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 4000000000
    shell:
        "diamond "
        "blastp "
        "--db {input.database} "
        "--more-sensitive "
        "--query {input.query} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "


rule combine_tair:
    input:
        expand(
            "blast_tair_out/blast.{chunk_id}.tair.blp",
            chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)])
    output:
        "blast."+PREFIX_QUERY+"_tair.blp"
    params:
        job_name = "Combining",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    shell:
        "find blast_tair_out/ -name \"*.blp\" -exec cat {{}} \; > {output}"



rule blast_trembl:
    input:
        database = TREMBL_DB+".dmnd",
        query = "tmp/fasta.{chunk_id}.fa",
    output:
        "blast_trembl_out/blast.{chunk_id}.trembl.blp"
    params:
        job_name = "Blofelding",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 4000000000
    shell:
        "diamond "
        "blastp "
        "--db {input.database} "
        "--more-sensitive "
        "--query {input.query} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "


rule combine_trembl:
    input:
        expand(
            "blast_trembl_out/blast.{chunk_id}.trembl.blp",
            chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)])
    output:
        "blast."+PREFIX_QUERY+"_trembl.blp"
    params:
        job_name = "Combining",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    shell:
        "find blast_trembl_out/ -name \"*.blp\" -exec cat {{}} \; > {output}"


rule run_interproscan:
    input:
        query = "tmp/fasta.{chunk_id}.fa",
    output:
        "interpro_out/interpro.{chunk_id}.out"
    params:
        job_name = "Interpro",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 4000000000
    shell:
        "/nfs/pgsb/commons/apps/interproscan5/interproscan-5.21-60.0/interproscan.sh "
        "-iprlookup "
        "-goterms "
        "-pa "
        "-o {output} "
        "-f TSV "
        "-dp "
        "-appl Pfam,TIGRFAM,SUPERFAMILY "
        "-i  {input.query} "


rule combine_interpro:
    input:
        expand(
            "interpro_out/interpro.{chunk_id}.out",
            chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)])
    output:
        "interpro."+PREFIX_QUERY+".out"
    params:
        job_name = "Combining",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    shell:
        "find interpro_out/ -name \"*.out\" -exec cat {{}} \; > {output}"


#create AHRD yaml file
rule write_yaml:
    input:
        #sprot = expand("blast_sprot_out/blast.{chunk_id}.sprot.blp",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        #tair = expand("blast_tair_out/blast.{chunk_id}.tair.blp",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        #trembl_plants = expand("blast_trembl_out/blast.{chunk_id}.trembl.blp",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        #ipscan = expand("interpro_out/interpro.{chunk_id}.out",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)])
        sprot = "blast_sprot_out/blast.{chunk_id}.sprot.blp",
        tair = "blast_tair_out/blast.{chunk_id}.tair.blp",
        trembl_plants = "blast_trembl_out/blast.{chunk_id}.trembl.blp",
        ipscan = "interpro_out/interpro.{chunk_id}.out"
    params:
        fasta = QUERY_NOSTAR,
        memory = "1G",
        job_name = "write_yaml",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    output:
        yaml = temp("yaml_out/"+ QUERY + ".{chunk_id}.run_ahrd.yaml"),
    run:
        with open(output.yaml,"w") as out:
            print("proteins_fasta: tmp/fasta.{0:05d}.fa".format(int(wildcards.chunk_id)),file = out)
            print("blast_dbs:",file = out)
            print("  sprot:",file = out)
            print("    weight: {0}".format(config['databases']['sprot']['weight']),file = out)
            print("    file: blast_sprot_out/blast.{0:05d}.sprot.blp".format(int(wildcards.chunk_id)),file = out)
            print("    database: {0}".format(config['databases']['sprot']['db']),file = out)
            print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
            print("    filter: {0}".format(config['databases']['sprot']['filter']),file = out)
            print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
            print("    description_score_bit_score_weight: {0}".format(config['databases']['sprot']['dsbs_weight']),file = out)
            print("  tair:",file = out)
            print("    weight: {0}".format(config['databases']['tair']['weight']),file = out)
            print("    file: blast_tair_out/blast.{0:05d}.tair.blp".format(int(wildcards.chunk_id)),file = out)
            print("    database: {0}".format(config['databases']['tair']['db']),file = out)
            print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
            print("    filter: {0}".format(config['databases']['tair']['filter']),file = out)
            print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
            print("    description_score_bit_score_weight: {0}".format(config['databases']['tair']['dsbs_weight']),file = out)
            print("    fasta_header_regex: ^>(?<accession>\S+)\s+(?<description>.+?)$",file = out)
            print("    short_accession_regex: ^(?<shortAccession>.+)$",file = out)
            print("  trembl_plants:",file = out)
            print("    weight: {0}".format(config['databases']['trembl_plants']['weight']),file = out)
            print("    file: blast_trembl_out/blast.{0:05d}.trembl.blp".format(int(wildcards.chunk_id)),file = out)
            print("    database: {0}".format(config['databases']['trembl_plants']['db']),file = out)
            print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
            print("    filter: {0}".format(config['databases']['trembl_plants']['filter']),file = out)
            print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
            print("    description_score_bit_score_weight: {0}".format(config['databases']['trembl_plants']['dsbs_weight']),file = out)
            print("interpro_database: {0}".format(config['databases']['ipscan']['db']),file = out)
            print("interpro_result: interpro_out/interpro.{0:05d}.out".format(int(wildcards.chunk_id)),file = out)
            print("gene_ontology_result: {0}".format(config['data']['gaf']),file = out)
            print("token_score_bit_score_weight: 0.5",file = out)
            print("token_score_database_score_weight: 0.3",file = out)
            print("token_score_overlap_score_weight: 0.2",file = out)
            print("output: ahrd_out/{0}.{1:05d}.ahrd.csv".format(QUERY,int(wildcards.chunk_id)),file = out)
            print("write_best_blast_hits_to_output: true",file = out)
            print("write_scores_to_output: true",file = out)


rule run_ahrd:
    input:
        yaml = "yaml_out/"+QUERY+ ".{chunk_id}.run_ahrd.yaml"
    params:
        fasta = QUERY_NOSTAR,
        memory = "16G",
        job_name = "run_ahrd",
        logo = WRK_DIR + "/ahrd.log.o",
        loge = WRK_DIR + "/ahrd.log.e",
    threads: 1
    resources:
        M = 16000000000
    output:
        csv = "ahrd_out/"+QUERY+".{chunk_id}.ahrd.csv"
    shell:
        "java -Xmx16g -jar /nfs/pgsb/commons/apps/AHRD/AHRD-3.3.3/AHRD/dist/ahrd.jar {input.yaml} "



rule combine_ahrd:
    input:
        expand("ahrd_out/"+QUERY+".{chunk_id}.ahrd.csv",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)])
    output:
        ahrd_out = "ahrd.combined.csv"
    params:
        job_name = "Combining",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    shell:
        "find ahrd_out/ -name \"*.ahrd.csv\" -exec cat {{}} \; > {output}"


rule parse_ahrd:
    input:
        ahrd_out = "ahrd.combined.csv"
    output:
        ahrd_out = PREFIX_QUERY+".ahrd.csv"
    params:
        memory = "1G",
        job_name = "parse_ahrd",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        M = 1000000000
    run:
        with open(output.ahrd_out,"w") as out:
            print("Protein-Accession	Blast-Hit-Accession	AHRD-Quality-Code	Human-Readable-Description	Interpro-ID (Description)	Gene-Ontology-Term	Best BlastHit against 'tair'	Best BlastHit against 'sprot'	Best BlastHit against 'trembl_plants'	Sum(Token-Scores)	TokenHighScore	Correction-Factor	Lexical-Score	RelativeBitScore\n", end='',file=out)
            with open(input.ahrd_out,"r") as fdh:
                for line in fdh:
                    if line.startswith("Protein") != True and line.startswith("# AHRD-Version 3.11") != True and line != "\n":
                        print(line.strip(),file=out)
