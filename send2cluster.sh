#!/bin/bash
#snakemake -s ahrd_v3.snakefile --cluster "qsub -N {params.job_name} -V -S /bin/bash -e {params.loge} -o {params.logo} -q plant2,plant_wheat -pe serial {threads} -l job_mem={resources.M}" --jobs 100 --restart-times 3 --latency-wait 300
snakemake -s ahrd_v5.snakefile --cluster "sbatch -p pgsb -N 1 -n {threads} -J {params.job_name} -e {params.loge} -o {params.logo} --exclude kodiac,barracuda,orca" --jobs 100 --restart-times 1 --latency-wait 120
