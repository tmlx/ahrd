#submit to cluster: snakemake -s ahrd_v4.snakefile --cluster "qsub -N {params.job_name} -V -S /bin/bash -e {params.loge} -o {params.logo} -q plant2,plant_wheat -pe serial {threads} -l job_mem={resources.M}" --jobs 100 --restart-times 3 --latency-wait 300
#submit to slurm: snakemake -s ahrd_v4.snakefile --cluster "sbatch -p pgsb -N 1 -n {threads} -J {params.job_name} -e {params.loge} -o {params.logo}" --jobs 100 --restart-times 1 --latency-wait 120
#submit to slurm: --reason --cluster "sbatch -p pgsb -N {resources.nodes} -n {threads} -J {params.job_name} -e {params.loge} -o {params.logo} --mem {resources.MB}" --jobs 100 --resources load=100 --restart-times 1 --latency-wait 120 --local-cores 4 --max-jobs-per-second 10 --max-status-checks-per-second 10
#submit to slurm: --reason --cluster "sbatch -p {resources.Q} -N {resources.nodes} -n {threads} -J {params.job_name} -e {params.loge} -o {params.logo} --mem {resources.MB}" --jobs 100 --resources load=100 --restart-times 1 --latency-wait 120 --local-cores 4 --max-jobs-per-second 10 --max-status-checks-per-second 10


#[25/10/2018] changed mem_mb to M and corrected the amount of memory requested from e.g. 1000 to 1000000000 (because it is passed along as bytes)
#[24/09/2019] added command line for slurm
#[26/09/2019] added rules for combining and parsing ahrd
#[27/07/2020] replaced blast with diamond
#[05/05/2021] changed to checkpoints
#[29/06/2021] some fixed and support for Q rescources

import Bio.SeqIO
from itertools import chain, islice


def chunks(iterable, size=10):
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, size - 1))

def aggregate_sprotblast(wildcards):
    checkpoint_output = checkpoints.chunk.get(**wildcards).output[0]
    file_names = expand("blast_sprot_out/blast.{i}.sprot.blp",i = glob_wildcards(os.path.join(checkpoint_output, "fasta.{i}.fa")).i)
    return file_names

def aggregate_tairblast(wildcards):
    checkpoint_output = checkpoints.chunk.get(**wildcards).output[0]
    file_names = expand("blast_tair_out/blast.{i}.tair.blp",i = glob_wildcards(os.path.join(checkpoint_output, "fasta.{i}.fa")).i)
    return file_names

def aggregate_tremblblast(wildcards):
    checkpoint_output = checkpoints.chunk.get(**wildcards).output[0]
    file_names = expand("blast_trembl_out/blast.{i}.trembl.blp",i = glob_wildcards(os.path.join(checkpoint_output, "fasta.{i}.fa")).i)
    return file_names

def aggregate_interpro(wildcards):
    checkpoint_output = checkpoints.chunk.get(**wildcards).output[0]
    file_names = expand("interpro_out/interpro.{i}.out",i = glob_wildcards(os.path.join(checkpoint_output, "fasta.{i}.fa")).i)
    return file_names

def aggregate_ahrd(wildcards):
    checkpoint_output = checkpoints.chunk.get(**wildcards).output[0]
    file_names = expand("ahrd_out/{i}.ahrd.csv",i = glob_wildcards(os.path.join(checkpoint_output, "fasta.{i}.fa")).i)
    return file_names



SPROT_DB = config["databases"]["sprot"]["db"]
TAIR_DB = config["databases"]["tair"]["db"]
TREMBL_DB = config["databases"]["trembl_plants"]["db"]
QUERY =    config["data"]["input"]
CHUNKS = config["data"]["chunks"]
# Qshort = ''.join(config["Q"]["short"]),
# Qlong = "pgsb",
# QXTRA = "--exclude kodiac,orca,barracuda",        
# print(type(Qshort),str(Qlong),*QXTRA)



BATCHES = int((len(list(Bio.SeqIO.parse(QUERY,"fasta")))/CHUNKS)+1)



PREFIX_QUERY = os.path.split(QUERY)[1]
QUERY_NOSTAR = PREFIX_QUERY+".nostar.fasta"
WRK_DIR = os.getcwd()
STDOUT =  WRK_DIR

# localrules: remove_stops, chunk

rule all:
    input: 
        aggregate_sprotblast,
        "blast."+PREFIX_QUERY+"_sprot.blp",
        "blast."+PREFIX_QUERY+"_tair.blp",
        "blast."+PREFIX_QUERY+"_trembl.blp",
        "interpro."+PREFIX_QUERY+".out",
        "ahrd."+PREFIX_QUERY+".out",
        PREFIX_QUERY+".ahrd.csv"
#        aggregate_ahrd,
#        "ahrd.combined.csv",
#         expand("ahrd_out/"+QUERY+".{chunk_id}.ahrd.csv", chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
#         PREFIX_QUERY+".ahrd.csv"

# removing any stop codon for interpro 


rule remove_stops:
    input:
        QUERY
    output:
        QUERY_NOSTAR
    params:
        job_name = "Prep",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        MB = 2000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["short"],
        XTRA = config["Q"]["XTRA"],        
    message: "Removing STOP codons"
    shell:
        "sed s'/*$//' "
        "{input} "
        "> {output}"


checkpoint chunk:
    input: QUERY_NOSTAR
    output:
        outdir = directory("tmp/")
    params:
        job_name = "Chunking",
        loge = WRK_DIR + "/chunking.log.e",
        logo = WRK_DIR + "/chunking.log.o",
    threads: 1
    resources:
        MB = 2000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["short"],
        XTRA = config["Q"]["XTRA"],        
    message: "Chunking input sequences"
    run:
        sio = Bio.SeqIO.parse(QUERY_NOSTAR, 'fasta')
        for cnt, chunk in enumerate(chunks(sio, CHUNKS)):
            with open("tmp/fasta.{0:05d}.fa".format(cnt), 'w') as out:
                Bio.SeqIO.write(chunk, out, 'fasta')


rule blast_sprot:
    input:
        database = SPROT_DB+".dmnd",
        query = "tmp/fasta.{i}.fa",
    output:
        "blast_sprot_out/blast.{i}.sprot.blp"
    params:
        job_name = "Blofelding",
        loge = WRK_DIR + "/sprot.log.e",
        logo = WRK_DIR + "/sprot.log.o",
    threads: 1
    resources:
        MB = 4000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["long"],
        XTRA = config["Q"]["XTRA"],        
    message: "Blofelding against sprot"
    shell:
        "diamond "
        "blastp "
        "--db {input.database} "
        "--more-sensitive "
        "--query {input.query} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "


rule blast_tair:
    input:
        database = TAIR_DB+".dmnd",
        query = "tmp/fasta.{i}.fa",
    output:
        "blast_tair_out/blast.{i}.tair.blp"
    params:
        job_name = "Blofelding",
        loge = WRK_DIR + "/tair.log.e",
        logo = WRK_DIR + "/tair.log.o",
    threads: 1
    resources:
        MB = 4000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["long"],
        XTRA = config["Q"]["XTRA"],        

    shell:
        "diamond "
        "blastp "
        "--db {input.database} "
        "--more-sensitive "
        "--query {input.query} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "


rule blast_trembl:
    input:
        database = TREMBL_DB+".dmnd",
        query = "tmp/fasta.{i}.fa",
    output:
        "blast_trembl_out/blast.{i}.trembl.blp"
    params:
        job_name = "Blofelding",
        loge = WRK_DIR + "/trembl.log.e",
        logo = WRK_DIR + "/trembl.log.o",
    threads: 1
    resources:
        MB = 4000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["long"],
        XTRA = config["Q"]["XTRA"],        
    shell:
        "diamond "
        "blastp "
        "--db {input.database} "
        "--more-sensitive "
        "--query {input.query} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "


rule run_interproscan:
    input:
        query = "tmp/fasta.{i}.fa",
    output:
        "interpro_out/interpro.{i}.out"
    params:
        job_name = "Interpro",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        MB = 16000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["long"],
        XTRA = config["Q"]["XTRA"],        
    shell:
        "/nfs/pgsb/commons/apps/interproscan5/interproscan-5.21-60.0/interproscan.sh "
        "-iprlookup "
        "-goterms "
        "-pa "
        "-o {output} "
        "-f TSV "
        "-dp "
        "-appl Pfam,TIGRFAM,SUPERFAMILY "
        "-i  {input.query} "







#create AHRD yaml file
rule write_yaml:
    input:
        #sprot = expand("blast_sprot_out/blast.{chunk_id}.sprot.blp",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        #tair = expand("blast_tair_out/blast.{chunk_id}.tair.blp",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        #trembl_plants = expand("blast_trembl_out/blast.{chunk_id}.trembl.blp",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)]),
        #ipscan = expand("interpro_out/interpro.{chunk_id}.out",chunk_id = ['{0:05d}'.format(x) for x in range(0, BATCHES)])
        sprot = "blast_sprot_out/blast.{i}.sprot.blp",
        tair = "blast_tair_out/blast.{i}.tair.blp",
        trembl_plants = "blast_trembl_out/blast.{i}.trembl.blp",
        ipscan = "interpro_out/interpro.{i}.out"
    params:
        fasta = QUERY_NOSTAR,
        memory = "1G",
        job_name = "write_yaml",
        loge = WRK_DIR + "/write_yaml.log.e",
        logo = WRK_DIR + "/write_yaml.log.o",
    threads: 1
    resources:
        MB = 2000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["long"],
        XTRA = config["Q"]["XTRA"],        
    output:
        yaml = "yaml_out/{i}.run_ahrd.yaml",
    run:
        with open(output.yaml,"w") as out:
            print("proteins_fasta: tmp/fasta.{0:05d}.fa".format(int(wildcards.i)),file = out)
            print("blast_dbs:",file = out)
            print("  sprot:",file = out)
            print("    weight: {0}".format(config['databases']['sprot']['weight']),file = out)
            print("    file: blast_sprot_out/blast.{0:05d}.sprot.blp".format(int(wildcards.i)),file = out)
            print("    database: {0}".format(config['databases']['sprot']['db']),file = out)
            print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
            print("    filter: {0}".format(config['databases']['sprot']['filter']),file = out)
            print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
            print("    description_score_bit_score_weight: {0}".format(config['databases']['sprot']['dsbs_weight']),file = out)
            print("  tair:",file = out)
            print("    weight: {0}".format(config['databases']['tair']['weight']),file = out)
            print("    file: blast_tair_out/blast.{0:05d}.tair.blp".format(int(wildcards.i)),file = out)
            print("    database: {0}".format(config['databases']['tair']['db']),file = out)
            print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
            print("    filter: {0}".format(config['databases']['tair']['filter']),file = out)
            print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
            print("    description_score_bit_score_weight: {0}".format(config['databases']['tair']['dsbs_weight']),file = out)
            print("    fasta_header_regex: ^>(?<accession>\S+)\s+(?<description>.+?)$",file = out)
            print("    short_accession_regex: ^(?<shortAccession>.+)$",file = out)
            print("  trembl_plants:",file = out)
            print("    weight: {0}".format(config['databases']['trembl_plants']['weight']),file = out)
            print("    file: blast_trembl_out/blast.{0:05d}.trembl.blp".format(int(wildcards.i)),file = out)
            print("    database: {0}".format(config['databases']['trembl_plants']['db']),file = out)
            print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
            print("    filter: {0}".format(config['databases']['trembl_plants']['filter']),file = out)
            print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
            print("    description_score_bit_score_weight: {0}".format(config['databases']['trembl_plants']['dsbs_weight']),file = out)
            print("interpro_database: {0}".format(config['databases']['ipscan']['db']),file = out)
            print("interpro_result: interpro_out/interpro.{0:05d}.out".format(int(wildcards.i)),file = out)
            print("gene_ontology_result: {0}".format(config['data']['gaf']),file = out)
            print("token_score_bit_score_weight: 0.5",file = out)
            print("token_score_database_score_weight: 0.3",file = out)
            print("token_score_overlap_score_weight: 0.2",file = out)
            print("output: ahrd_out/{0:05d}.ahrd.csv".format(int(wildcards.i)),file = out)
            print("write_best_blast_hits_to_output: true",file = out)
            print("write_scores_to_output: true",file = out)


rule run_ahrd:
    input:
        yaml = "yaml_out/{i}.run_ahrd.yaml"
    params:
        fasta = QUERY_NOSTAR,
        memory = "16G",
        job_name = "run_ahrd",
        logo = WRK_DIR + "/ahrd.log.o",
        loge = WRK_DIR + "/ahrd.log.e",
    threads: 1
    resources:
        MB = 16000,
        nodes = 1,
        load = 2,
        threads = 1,
        Q = config["Q"]["long"],
        XTRA = config["Q"]["XTRA"],        
    output:
        csv = "ahrd_out/{i}.ahrd.csv"
    shell:
        "java -Xmx16g -jar /nfs/pgsb/commons/apps/AHRD/AHRD-3.3.3/AHRD/dist/ahrd.jar {input.yaml} "


rule combine_all:
    input:
        aggregate_sprotblast,
        aggregate_tairblast,
        aggregate_tremblblast,
        aggregate_interpro,
        aggregate_ahrd,
    output:
        sprot = "blast."+PREFIX_QUERY+"_sprot.blp",
        tair = "blast."+PREFIX_QUERY+"_tair.blp",        
        trembl = "blast."+PREFIX_QUERY+"_trembl.blp",
        interpro = "interpro."+PREFIX_QUERY+".out",
        ahrd = "ahrd."+PREFIX_QUERY+".out",
    params:
        job_name = "Combining",
        loge = WRK_DIR + "/combine.log.e",
        logo = WRK_DIR + "/combine.log.o",
    threads: 1
    resources:
        MB = 2000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["long"],
        XTRA = config["Q"]["XTRA"],        
    run:
        shell("find blast_sprot_out/ -name \"*.blp\" -exec cat {{}} \; > {output.sprot}")
        shell("find blast_tair_out/ -name \"*.blp\" -exec cat {{}} \; > {output.tair}")
        shell("find blast_trembl_out/ -name \"*.blp\" -exec cat {{}} \; > {output.trembl}")
        shell("find interpro_out/ -name \"*.out\" -exec cat {{}} \; > {output.interpro}")
        shell("find ahrd_out/ -name \"*.ahrd.csv\" -exec cat {{}} \; > {output.ahrd}")



rule parse_ahrd:
    input:
        ahrd_out = "ahrd."+PREFIX_QUERY+".out",
    output:
        ahrd_out = PREFIX_QUERY+".ahrd.csv"
    params:
        memory = "1G",
        job_name = "parse_ahrd",
        loge = WRK_DIR + "/ahrd.log.e",
        logo = WRK_DIR + "/ahrd.log.o",
    threads: 1
    resources:
        MB = 2000,
        nodes = 1,
        load = 1,
        threads = 1,
        Q = config["Q"]["long"],
        XTRA = config["Q"]["XTRA"],        
    run:
        with open(output.ahrd_out,"w") as out:
            print("Protein-Accession	Blast-Hit-Accession	AHRD-Quality-Code	Human-Readable-Description	Interpro-ID (Description)	Gene-Ontology-Term	Best BlastHit against 'tair'	Best BlastHit against 'sprot'	Best BlastHit against 'trembl_plants'	Sum(Token-Scores)	TokenHighScore	Correction-Factor	Lexical-Score	RelativeBitScore\n", end='',file=out)
            with open(input.ahrd_out,"r") as fdh:
                for line in fdh:
                    if line.startswith("Protein") != True and line.startswith("# AHRD-Version 3.11") != True and line != "\n":
                        print(line.strip(),file=out)
